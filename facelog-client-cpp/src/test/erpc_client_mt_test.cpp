/*
* test_erpcdemo_client.c
*
*  Created on: Apr 14, 2020
*      Author: guyadong
*/

#include <vector>
#include <string.h>
#include <iostream>
#include <erpc_client_setup.h>
#include <erpc_port.h>
#include <facelog_erpc_output.h>
#include <facelog_erpc_mem.h>
#include <erpc_setup_client_tcp.h>
#include <ThreadPool.h>
#include <sample_log.h>
#include "ErpcClientCmdParam.h"

using namespace std;
using namespace gdface;
using namespace net::gdface::utils;

int main(int argc, char *argv[])
{
	ErpcClientConfig param;
	param.parse(argc, argv);
	std::cout <<"======connect facelog eRPC proxy service:" << param.host <<  ":" << param.port << std::endl;

	/* 创建client端传输层对象(TCP) */
	auto transport = erpc_transport_client_tcp_init(param.host.c_str(), param.port,0,0,0);
	auto message_buffer_factory = erpc_mbf_dynamic_init();
	/* 初始化客户端 */
	erpc_client_init(transport, message_buffer_factory);

	ThreadPool thread_pool(std::thread::hardware_concurrency());
	try{
		// 运算次数
		int runCount = 200;
		std::vector< std::future<void> > futures(runCount);
		for (int i = 0; i < runCount; ++i) {
			// 向线程池加入任务
			futures[i] = thread_pool.enqueue([&,i]() {
				std::string errmsg= std::string(256,'\0');	
				/* RPC 调用 fl_getDevice */
				DeviceBean_t _return;
				init_DeviceBean_t_ref(_return);
				auto resp = fl_getDevice((char*)errmsg.data(),&_return,37);
				cout << "<" << i << ">" <<" return code:" << resp << endl;
				if(resp){
					cout << "error message:" << errmsg << endl;
				}else{
					/* 输出返回值 */
					cout << "fl_getDevice response:" << _return << endl;
				}
				free_DeviceBean_t_ref(_return);
			});
		}
		// 等待所有线程结束
		for (auto &f: futures) {
			f.get();
		}
	}
	catch (std::exception& tx) {
		SAMPLE_ERR("ERROR: {}", tx.what());
	}
	/* 关闭socket */
	erpc_transport_client_tcp_deinit();
}
