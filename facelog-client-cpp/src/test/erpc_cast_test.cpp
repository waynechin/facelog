#include <iostream>
#include <facelog_erpc_cast.h>
using namespace net::gdface::utils;
using namespace ::gdface;
void test() {
	std::vector<std::string> v;
	std::vector<int32_t> vi32;
	std::string str;
	list_string_1_t ls;
	list_binary_1_t lb;
	list_int32_1_t li32;
	cast(v, ls);
	cast(v, lb);
	cast(lb, v);
	cast(ls, v);
	cast(vi32, li32);
	cast(li32, vi32);
	std::cout << "has_member_elements<list_string_1_t>:" << (has_member_elements<list_string_1_t>::value && has_member_elementsCount<list_string_1_t>::value) << std::endl;
	std::cout << "is_list_struct<list_string_1_t>:" << is_list_struct<list_string_1_t>::value << std::endl;
	std::cout << "is_list_struct<std::string>:" << is_list_struct<std::string>::value << std::endl;
	std::cout << "is_list_struct<list_binary_1_t>:" << is_list_struct<list_binary_1_t>::value << std::endl;
	std::cout << "is_binary<std::string>:" << is_binary<std::string>::value << std::endl;
	std::cout << "is_binary<binary_t>:" << is_binary<binary_t>::value << std::endl;
	std::cout << "is_list_binary<std::string>:" << is_list_binary<std::string>::value << std::endl;
	std::cout << "is_list_binary<list_binary_1_t>:" << is_list_binary<list_binary_1_t>::value << std::endl;
	auto b = is_list_string<list_string_1_t>::value;
	std::cout << "b:" << b << std::endl;
	auto c = is_list_container<decltype(v)>::value;
	auto d = is_list_container<std::string>::value;
	auto e = is_list_struct<std::string>::value;
	std::cout << "e:" << e << std::endl;
	auto f = is_list_string<std::string>::value;
	std::cout << "f:" << f << std::endl;
}
int main(int argc, char *argv[]) {
	ErrorLogBean errorLogBean_cxx;
	ErrorLogBean_t errorLogBean_c;
	cast(errorLogBean_c, errorLogBean_cxx);
	cast(errorLogBean_c, &errorLogBean_cxx);
}