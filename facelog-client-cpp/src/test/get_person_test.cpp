/*
* get_person_test.cpp
* client连接测试
*  Created on: 2020年03月17日
*      Author: guyadong
*/
#include <iostream>
#include <nlohmann/json.hpp>
#include "sample_log.h"
#include "IFaceLogIfClient.h"
#include "BaseCmdParam.h"

using namespace net::gdface::facelog;
using namespace gdface;
using namespace codegen;
using namespace nlohmann;
using namespace std;

void test_getPerson(IFaceLogIfClient &client) {
	int person_id = 1;
	auto p = client.getPerson(person_id);
	if (p) {
		auto &person = *p;
		cout << person << endl;
		json j = person;
		cout << "person's json=" << j << endl;
		auto p2 = j.get<db::PersonBean>();
		cout << "person bean from json:" << endl;
		cout << p2 << endl;
	}
	else {
		cout << "not found person bean for " << person_id << endl;
	}
}

void test_loadPersonByWhere(IFaceLogIfClient &client) {
	cout << "\ntest_loadPersonByWhere\n" ;
	int person_id = 1;
	auto p = client.loadPersonByWhere(std::string(""),1,-1);
	if (p) {
		auto person_ids = *p;
		int c = 0;
		for (auto e : person_ids) {
			cout << c << " person e: " << e << endl;
		}
	}
	else {
		cout << "not found person bean for " << person_id << endl;
	}
}

int main(int argc, char *argv[]) {
	BaseClientConfig param;
	param.parse(argc, argv);

	auto client = IFaceLogIfClient(param.host, param.port);

	try {
		SAMPLE_OUT("connect service:{}@{} ...", param.port, param.host);
		test_getPerson(client);
		//test_loadPersonByWhere(client);
	}
	catch (std::exception& tx) {
		SAMPLE_ERR("ERROR: {}{}", typeid(tx).name(),tx.what());
	}
}