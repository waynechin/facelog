// ______________________________________________________
// Generated by codegen - https://gitee.com/l0km/codegen 
// template: thrift_cpp/erpc_proxy/perservice/interface_proxy_impl.cpp.vm
// ______________________________________________________ 
/**
 * 基于 thrift stub 的 IFaceLog  接口eRPC代理实现(C++11) <br>
 * C++11 compiler or VS2015 or above required <br>
 * 计算机生成代码(generated by automated tools ThriftServiceDecoratorGenerator @author guyadong)<br>
 */
#include <string.h>
#include <iostream>
#include <memory>
#include <thread>
#include <raii.h>
#include <assert_macros.h>
#include <IFaceLogTlsProvider.h>
#include "facelog.h"
#include "facelog_server.h"
#include "facelog_erpc_cast.h"
#include "facelog_erpc_output.h"
#include "facelog_erpc_mem.h"

using namespace net::gdface::utils;
using namespace net::gdface::facelog;
extern std::shared_ptr<IFaceLogClientProviderIf> clientProvider;
extern bool debug;

static void fill_error_message(const std::exception *e,char *_output,size_t output_size)
{
  if(_output && output_size > 0){
    std::snprintf((char*)_output,output_size,"%s:%s",typeid(e).name(),e->what());
  }
  if(debug){
    std::printf("%s:%s\n",typeid(e).name(),e->what());
  }
}

// 创建TTransport的RAII对象，用于自动执行open/close动作
static ::gdface::raii raii_transport(const std::shared_ptr<IFaceLogClientProviderIf>& provider)
{
	return ::gdface::make_raii(provider->getTransport(),
		&::apache::thrift::transport::TTransport::close,
		&::apache::thrift::transport::TTransport::open);
}
ErpcProxyReturnCode_t fl_addErrorLog(char * _err_msg, const ErrorLogBean_t * errorLogBean, const Token_t * token)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_addErrorLog input parameters:" << std::endl;
    std::cout << "errorLogBean=[" << errorLogBean << "]" << std::endl;
    std::cout << "token=[" << token << "]" << std::endl;
  }
  try{
    throw_if_null(_err_msg);
    _err_msg[0] = '\0';
    auto raii = raii_transport(clientProvider);
    clientProvider->get().addErrorLog(      cast(errorLogBean,(::gdface::ErrorLogBean *)nullptr),
      cast(token,(::gdface::Token *)nullptr));
    if(debug){
      std::cout << "fl_addErrorLog NO return" << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_addLog(char * _err_msg, const LogBean_t * logBean, const Token_t * token)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_addLog input parameters:" << std::endl;
    std::cout << "logBean=[" << logBean << "]" << std::endl;
    std::cout << "token=[" << token << "]" << std::endl;
  }
  try{
    throw_if_null(_err_msg);
    _err_msg[0] = '\0';
    auto raii = raii_transport(clientProvider);
    clientProvider->get().addLog(      cast(logBean,(::gdface::LogBean *)nullptr),
      cast(token,(::gdface::Token *)nullptr));
    if(debug){
      std::cout << "fl_addLog NO return" << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::DuplicateRecordException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_DuplicateRecordException_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_createTempPwd(char * _err_msg, char * _return, int32_t targetId, TmpPwdTargetType_t targetType, const char * expiryDate, const Token_t * token)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_createTempPwd input parameters:" << std::endl;
    std::cout << "targetId=[" << targetId << "]" << std::endl;
    std::cout << "targetType=[" << targetType << "]" << std::endl;
    if(expiryDate)
    {
      std::cout << "expiryDate=[" << expiryDate << "]" << std::endl;
    }
    else
    {
      std::cout << "expiryDate=[<null>]" << std::endl;
    }
    std::cout << "token=[" << token << "]" << std::endl;
  }
  std::string _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    _return[0] = '\0';
    auto raii = raii_transport(clientProvider);
    clientProvider->get().createTempPwd(_r,
      cast(targetId,(int32_t *)nullptr),
      cast(targetType,(::gdface::TmpPwdTargetType::type *)nullptr),
      cast(expiryDate,(std::string *)nullptr),
      cast(token,(::gdface::Token *)nullptr));
    cast(_r,_return,DEFAULT_MAX_LENGTH);
    if(debug){
      std::cout << "fl_createTempPwd return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_createTempPwdWithDuration(char * _err_msg, char * _return, int32_t targetId, TmpPwdTargetType_t targetType, int32_t duration, const Token_t * token)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_createTempPwdWithDuration input parameters:" << std::endl;
    std::cout << "targetId=[" << targetId << "]" << std::endl;
    std::cout << "targetType=[" << targetType << "]" << std::endl;
    std::cout << "duration=[" << duration << "]" << std::endl;
    std::cout << "token=[" << token << "]" << std::endl;
  }
  std::string _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    _return[0] = '\0';
    auto raii = raii_transport(clientProvider);
    clientProvider->get().createTempPwdWithDuration(_r,
      cast(targetId,(int32_t *)nullptr),
      cast(targetType,(::gdface::TmpPwdTargetType::type *)nullptr),
      cast(duration,(int32_t *)nullptr),
      cast(token,(::gdface::Token *)nullptr));
    cast(_r,_return,DEFAULT_MAX_LENGTH);
    if(debug){
      std::cout << "fl_createTempPwdWithDuration return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_faceRecognizePersonPermitted(char * _err_msg, int32_t * _return, const binary_t * imageData, double threshold, int32_t group, int32_t deviceId, bool searchInPermited)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_faceRecognizePersonPermitted input parameters:" << std::endl;
    std::cout << "imageData=[" << imageData << "]" << std::endl;
    std::cout << "threshold=[" << threshold << "]" << std::endl;
    std::cout << "group=[" << group << "]" << std::endl;
    std::cout << "deviceId=[" << deviceId << "]" << std::endl;
    std::cout << "searchInPermited=[" << searchInPermited << "]" << std::endl;
  }
  int32_t _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    auto raii = raii_transport(clientProvider);
    _r = clientProvider->get().faceRecognizePersonPermitted(      cast(imageData,(std::string *)nullptr),
      cast(threshold,(double *)nullptr),
      cast(group,(int32_t *)nullptr),
      cast(deviceId,(int32_t *)nullptr),
      cast(searchInPermited,(bool *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_faceRecognizePersonPermitted return :" << *_return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_fuzzySearch(char * _err_msg, list_MatchEntry_t_1_t * _return, const char * tablename, const char * column, const char * pattern, StringMatchType_t matchType, int32_t matchFlags, int32_t parentGroupId, int32_t maxMatchCount)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_fuzzySearch input parameters:" << std::endl;
    if(tablename)
    {
      std::cout << "tablename=[" << tablename << "]" << std::endl;
    }
    else
    {
      std::cout << "tablename=[<null>]" << std::endl;
    }
    if(column)
    {
      std::cout << "column=[" << column << "]" << std::endl;
    }
    else
    {
      std::cout << "column=[<null>]" << std::endl;
    }
    if(pattern)
    {
      std::cout << "pattern=[" << pattern << "]" << std::endl;
    }
    else
    {
      std::cout << "pattern=[<null>]" << std::endl;
    }
    std::cout << "matchType=[" << matchType << "]" << std::endl;
    std::cout << "matchFlags=[" << matchFlags << "]" << std::endl;
    std::cout << "parentGroupId=[" << parentGroupId << "]" << std::endl;
    std::cout << "maxMatchCount=[" << maxMatchCount << "]" << std::endl;
  }
  std::vector<::gdface::MatchEntry> _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_list_MatchEntry_t_1_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().fuzzySearch(_r,
      cast(tablename,(std::string *)nullptr),
      cast(column,(std::string *)nullptr),
      cast(pattern,(std::string *)nullptr),
      cast(matchType,(::gdface::StringMatchType::type *)nullptr),
      cast(matchFlags,(int32_t *)nullptr),
      cast(parentGroupId,(int32_t *)nullptr),
      cast(maxMatchCount,(int32_t *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_fuzzySearch return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::FuzzyMatchCountExceedLimitException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FuzzyMatchCountExceedLimitException_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_getFeatureBytesList(char * _err_msg, list_binary_1_t * _return, const list_string_1_t * md5List, bool truncation)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_getFeatureBytesList input parameters:" << std::endl;
    std::cout << "md5List=[" << md5List << "]" << std::endl;
    std::cout << "truncation=[" << truncation << "]" << std::endl;
  }
  std::vector<std::string> _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_list_binary_1_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().getFeatureBytesList(_r,
      cast(md5List,(std::vector<std::string> *)nullptr),
      cast(truncation,(bool *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_getFeatureBytesList return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_getFeatureBytesTruncation(char * _err_msg, binary_t * _return, const char * md5, bool truncation)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_getFeatureBytesTruncation input parameters:" << std::endl;
    if(md5)
    {
      std::cout << "md5=[" << md5 << "]" << std::endl;
    }
    else
    {
      std::cout << "md5=[<null>]" << std::endl;
    }
    std::cout << "truncation=[" << truncation << "]" << std::endl;
  }
  std::string _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_binary_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().getFeatureBytesTruncation(_r,
      cast(md5,(std::string *)nullptr),
      cast(truncation,(bool *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_getFeatureBytesTruncation return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_getFeaturesByPersonIdAndSdkVersion(char * _err_msg, list_string_1_t * _return, int32_t personId, const char * sdkVersion)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_getFeaturesByPersonIdAndSdkVersion input parameters:" << std::endl;
    std::cout << "personId=[" << personId << "]" << std::endl;
    if(sdkVersion)
    {
      std::cout << "sdkVersion=[" << sdkVersion << "]" << std::endl;
    }
    else
    {
      std::cout << "sdkVersion=[<null>]" << std::endl;
    }
  }
  std::vector<std::string> _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_list_string_1_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().getFeaturesByPersonIdAndSdkVersion(_r,
      cast(personId,(int32_t *)nullptr),
      cast(sdkVersion,(std::string *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_getFeaturesByPersonIdAndSdkVersion return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_getPersonsPermittedOnDevice(char * _err_msg, list_int32_1_t * _return, int32_t deviceId, bool ignoreSchedule, const list_int32_1_t * excludePersonIds, int64_t timestamp)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_getPersonsPermittedOnDevice input parameters:" << std::endl;
    std::cout << "deviceId=[" << deviceId << "]" << std::endl;
    std::cout << "ignoreSchedule=[" << ignoreSchedule << "]" << std::endl;
    std::cout << "excludePersonIds=[" << excludePersonIds << "]" << std::endl;
    std::cout << "timestamp=[" << timestamp << "]" << std::endl;
  }
  std::vector<int32_t> _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_list_int32_1_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().getPersonsPermittedOnDevice(_r,
      cast(deviceId,(int32_t *)nullptr),
      cast(ignoreSchedule,(bool *)nullptr),
      cast(excludePersonIds,(std::vector<int32_t> *)nullptr),
      cast(timestamp,(int64_t *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_getPersonsPermittedOnDevice return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_getTargetInfo4PwdOnDevice(char * _err_msg, TmpwdTargetInfo_t * _return, const char * pwd, const Token_t * token)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_getTargetInfo4PwdOnDevice input parameters:" << std::endl;
    if(pwd)
    {
      std::cout << "pwd=[" << pwd << "]" << std::endl;
    }
    else
    {
      std::cout << "pwd=[<null>]" << std::endl;
    }
    std::cout << "token=[" << token << "]" << std::endl;
  }
  ::gdface::TmpwdTargetInfo _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_TmpwdTargetInfo_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().getTargetInfo4PwdOnDevice(_r,
      cast(pwd,(std::string *)nullptr),
      cast(token,(::gdface::Token *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_getTargetInfo4PwdOnDevice return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_iso8601Time(char * _err_msg, char * _return)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_iso8601Time NO input parameters" << std::endl;
  }
  std::string _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    _return[0] = '\0';
    auto raii = raii_transport(clientProvider);
    clientProvider->get().iso8601Time(_r);
    cast(_r,_return,DEFAULT_MAX_LENGTH);
    if(debug){
      std::cout << "fl_iso8601Time return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_lockWakeup(char * _err_msg, LockWakeupResponse_t * _return, const DeviceBean_t * deviceBean, bool ignoreSchedule, const char * sdkVersion)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_lockWakeup input parameters:" << std::endl;
    std::cout << "deviceBean=[" << deviceBean << "]" << std::endl;
    std::cout << "ignoreSchedule=[" << ignoreSchedule << "]" << std::endl;
    if(sdkVersion)
    {
      std::cout << "sdkVersion=[" << sdkVersion << "]" << std::endl;
    }
    else
    {
      std::cout << "sdkVersion=[<null>]" << std::endl;
    }
  }
  ::gdface::LockWakeupResponse _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_LockWakeupResponse_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().lockWakeup(_r,
      cast(deviceBean,(::gdface::DeviceBean *)nullptr),
      cast(ignoreSchedule,(bool *)nullptr),
      cast(sdkVersion,(std::string *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_lockWakeup return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceSecurityException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceSecurityException_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_online(char * _err_msg, Token_t * _return, const DeviceBean_t * device)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_online input parameters:" << std::endl;
    std::cout << "device=[" << device << "]" << std::endl;
  }
  ::gdface::Token _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_Token_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().online(_r,
      cast(device,(::gdface::DeviceBean *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_online return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceSecurityException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceSecurityException_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_registerDevice(char * _err_msg, DeviceBean_t * _return, const DeviceBean_t * newDevice)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_registerDevice input parameters:" << std::endl;
    std::cout << "newDevice=[" << newDevice << "]" << std::endl;
  }
  ::gdface::DeviceBean _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_DeviceBean_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().registerDevice(_r,
      cast(newDevice,(::gdface::DeviceBean *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_registerDevice return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceSecurityException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceSecurityException_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}
ErpcProxyReturnCode_t fl_updateDevice(char * _err_msg, DeviceBean_t * _return, const DeviceBean_t * deviceBean, const Token_t * token)
{
  if(debug){
    std::cout << "thread id:" << std::this_thread::get_id() << " ";
    std::cout << "fl_updateDevice input parameters:" << std::endl;
    std::cout << "deviceBean=[" << deviceBean << "]" << std::endl;
    std::cout << "token=[" << token << "]" << std::endl;
  }
  ::gdface::DeviceBean _r;
  try{
    throw_if_null(_err_msg);
    throw_if_null(_return);
    _err_msg[0] = '\0';
    init_DeviceBean_t(_return);
    auto raii = raii_transport(clientProvider);
    clientProvider->get().updateDevice(_r,
      cast(deviceBean,(::gdface::DeviceBean *)nullptr),
      cast(token,(::gdface::Token *)nullptr));
    cast(_r,*_return);
    if(debug){
      std::cout << "fl_updateDevice return :" << _return << std::endl;
    }
    return ErpcProxyReturnCode_SUCCESS_c;
  }
  catch(::gdface::ServiceRuntimeException &e){
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_ServiceRuntimeException_c;
  }
  catch (::apache::thrift::TApplicationException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    if (e.getType() == ::apache::thrift::TApplicationException::TApplicationExceptionType::MISSING_RESULT) {
      return ErpcProxyReturnCode_EMPTY_REPLY_c;
    }
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (::apache::thrift::TException &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_THRIFT_ERROR_c;
  }
  catch (std::bad_alloc &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_MEMORY_ERROR_c;
  }
  catch (std::invalid_argument &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_INVALID_ARGUMENT_c;
  }
  catch (std::exception &e) {
    fill_error_message(&e,_err_msg,ERRMSG_MAX_LENGTH);
    return ErpcProxyReturnCode_FAIL_c;
  }
  catch (...) {
    return ErpcProxyReturnCode_RUNTIME_ERROR_c;
  }
}