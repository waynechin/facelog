#!/bin/bash
# 生成 IFaceLog cpp client代码脚本
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd "$sh_folder"
OUT_FOLDER=$sh_folder/src/client/stub
# 指定thrift compiler位置
[ $(which thrift) >/dev/null ] && THRIFT_EXE=thrift
[ -z "$THRIFT_EXE" ] && THRIFT_EXE=$sh_folder/dependencies/dist/thrift-$(g++ -dumpmachine)/bin/thrift
$THRIFT_EXE --version || exit
	
if [ -e "$OUT_FOLDER" ] ;
then
	rm  "$OUT_FOLDER"/IFaceLog_types.* 1>/dev/null 2>/dev/null
	rm  "$OUT_FOLDER"/IFaceLog.* 1>/dev/null 2>/dev/null
	rm  "$OUT_FOLDER"/IFaceLog_constants.* 1>/dev/null 2>/dev/null
else
	mkdir -p "$OUT_FOLDER"
fi

$THRIFT_EXE --gen cpp:no_skeleton,moveable_types,templates \
	-out "$OUT_FOLDER" \
	$sh_folder/../facelog-service/IFaceLog.thrift || exit

popd
