#!/bin/bash
# linux下编译thrift
# 需要依赖的工具和库
# sudo apt-get install automake bison flex g++ git libboost-all-dev libevent-dev libssl-dev libtool make pkg-config

sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder

[ -n "$1" ] && install_prefix=$1
[ -z "$1" ] && install_prefix=$sh_folder/dist/linux_thrift-$(arch)
[ -e "$install_prefix" ] && rm -fr "$install_prefix"

[ -n "$2" ] && [ -d "$2" ] && thrift_folder=$2
[ -z "$2" ] && [ -d thrift ] && thrift_folder=thrift
[ -z "$2" ] && [ -d thrift-0.11.0 ] && thrift_folder=thrift-0.11.0
[ -z "$thrift_folder" ] && echo "invalid thrift_folder" && exit -1

pushd $thrift_folder

if [ ! -x configure ] 
then
  ./bootstrap.sh || exit -1
fi
[ -f Makefile ] && make clean && make distclean
# 生成Makefile
./configure  --enable-shared=no \
        --enable-tests=no \
        --enable-tutorial=no \
        --with-c_glib=no \
        --with-cpp=yes \
        --with-java=no \
        --with-qt4=no \
        --with-qt5=no \
        --with-csharp=no \
        --with-erlang=no \
        --with-lua=no \
        --with-python=no \
        --with-perl=no \
        --with-php=no \
        --with-php_extension=no \
        --with-dart=no \
        --with-ruby=no \
        --with-haskell=no \
        --with-go=no \
        --with-rs=no \
        --with-haxe=no \
        --with-dotnetcore=no \
        --with-nodejs=no \
        --with-d=no \
        --prefix=$install_prefix || exit -1

make -j8 install

popd
popd
