#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
folder_name=$(basename $sh_folder) 

CXX=arm-linux-gnueabihf-g++
[ -n "$CROSS_COMPILER_PREFIX" ] && CXX=$CROSS_COMPILER_PREFIX/$CXX

[ ! $($CXX -dumpmachine) ] \
    && echo "not install compiler arm-linux-gnueabihf-g++,install please:" \
    && echo "sudo apt-get install g++-arm-linux-gnueabihf" \
    && echo "sudo apt-get install gcc-arm-linux-gnueabihf" \
    && exit 255

prj_folder=$sh_folder/../$folder_name.arm

pushd $sh_folder
OUTPUT_DEBUG_LOG=ON \
TOOLCHAIN_FILE=$sh_folder/cmake/armhf-linux-gnueabihf.toolchain.cmake \
PROJECT_FOLDER=$prj_folder \
BUILD_TYPE=RELEASE \
PREFIX=$sh_folder/../dist/erpc-$($CXX -dumpmachine) \
./make_unix_makefile.sh || exit 255

cmake --build $prj_folder --target clean || exit 255
cmake --build $prj_folder --target install -- -j8

popd
