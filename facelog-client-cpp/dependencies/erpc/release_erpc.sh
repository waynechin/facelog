#!/bin/bash
# 编译release版本
sh_folder=$(cd "$(dirname $0)"; pwd -P)
folder_name=$(basename $sh_folder) 

pushd $sh_folder

OUTPUT_DEBUG_LOG=ON \
BUILD_TYPE=RELEASE \
./make_unix_makefile.sh || exit 255
cmake --build ../$folder_name.gnu --target clean
cmake --build ../$folder_name.gnu --target install -- -j8

popd
