/*
 * Generated by erpcgen 1.7.3.
 *
 * AUTOGENERATED - DO NOT EDIT
 */


#include "erpcdemo_server.h"
#include <new>
#include "erpc_port.h"
#include "erpc_manually_constructed.h"
#include <stdio.h>

#if 10703 != ERPC_VERSION_NUMBER
#error "The generated shim code version is different to the rest of eRPC code."
#endif

using namespace erpc;
using namespace std;

#if ERPC_NESTED_CALLS_DETECTION
extern bool nestingDetection;
#endif

static ManuallyConstructed<DEMO_service> s_DEMO_service;


//! @brief Function to write struct binary_t
static void write_binary_t_struct(erpc::Codec * codec, const binary_t * data);


// Write struct binary_t function implementation
static void write_binary_t_struct(erpc::Codec * codec, const binary_t * data)
{
    codec->writeBinary(data->dataLength, data->data);
}


//! @brief Function to free space allocated inside struct binary_t
static void free_binary_t_struct(binary_t * data);


// Free space allocated inside struct binary_t function implementation
static void free_binary_t_struct(binary_t * data)
{
    if (data->data)
    {
        erpc_free(data->data);
    }
}



// Call the correct server shim based on method unique ID.
erpc_status_t DEMO_service::handleInvocation(uint32_t methodId, uint32_t sequence, Codec * codec, MessageBufferFactory *messageFactory)
{
    switch (methodId)
    {
        case kDEMO_RD_demoHello_id:
            return RD_demoHello_shim(codec, messageFactory, sequence);

        default:
            return kErpcStatus_InvalidArgument;
    }
}

// Server shim for RD_demoHello of DEMO interface.
erpc_status_t DEMO_service::RD_demoHello_shim(Codec * codec, MessageBufferFactory *messageFactory, uint32_t sequence)
{
    erpc_status_t err = kErpcStatus_Success;

    binary_t *_return = NULL;
    char * txInput = NULL;
    callStatus_t result;

    // startReadMessage() was already called before this shim was invoked.

    uint32_t txInput_len;
    char * txInput_local;
    codec->readString(&txInput_len, &txInput_local);
    txInput = (char *) erpc_malloc((txInput_len + 1) * sizeof(char));
    if (txInput == NULL)
    {
        codec->updateStatus(kErpcStatus_MemoryError);
    }
    else
    {
        memcpy(txInput, txInput_local, txInput_len);
        (txInput)[txInput_len] = 0;
    }

    _return = (binary_t *) erpc_malloc(sizeof(binary_t));
    if (_return == NULL)
    {
        codec->updateStatus(kErpcStatus_MemoryError);
    }

    err = codec->getStatus();
    if (!err)
    {
        // Invoke the actual served function.
#if ERPC_NESTED_CALLS_DETECTION
        nestingDetection = true;
#endif
        result = _RD_demoHello(_return, txInput);
#if ERPC_NESTED_CALLS_DETECTION
        nestingDetection = false;
#endif
        // preparing MessageBuffer for serializing data
        err = messageFactory->prepareServerBufferForSend(codec->getBuffer());
    }

    if (!err)
    {
        // preparing codec for serializing data
        codec->reset();
        // Build response message.
        codec->startWriteMessage(kReplyMessage, kDEMO_service_id, kDEMO_RD_demoHello_id, sequence);

        write_binary_t_struct(codec, _return);

        codec->write(static_cast<int32_t>(result));

        err = codec->getStatus();
    }

    if (_return)
    {
        free_binary_t_struct(_return);
    }
    if (_return)
    {
        erpc_free(_return);
    }

    if (txInput)
    {
        erpc_free(txInput);
    }
    return err;
}

erpc_service_t create_DEMO_service()
{
    s_DEMO_service.construct();
    return s_DEMO_service.get();
}

void destroy_DEMO_service()
{
    s_DEMO_service.destroy();
}
