#include <string.h>
#include <iostream>
#include <memory>
#include <common_utilits.h>
#include <assert_macros.h>
#include "erpcdemo.h"
#include "erpcdemo_server.h"

using namespace std;
using namespace gdface::com_utilits;
#ifdef binary_t_DEFINED
static void
alloc_binary_t_ref(binary_t &obj, size_t size)
{
  if (nullptr == obj.data) {
    obj.data = (decltype(obj.data))malloc(size);
    if (obj.data) {
      obj.dataLength = (decltype(obj.dataLength))size;
    }
  }
  else if (obj.dataLength != size) {
    auto p = (decltype(obj.data))realloc(obj.data, size);
    if (p) {
      obj.data = p;
      obj.dataLength = (decltype(obj.dataLength))size;
    }
  }
}
static void
cast(const std::string &left, binary_t &right)
{
  alloc_binary_t_ref(right, left.size());
  throw_except_if_false(std::bad_alloc, (nullptr != right.data) && (left.size() == (size_t)right.dataLength));
  if (right.data) {
    std::memcpy(right.data, left.data(), (size_t)right.dataLength);
  }
}
#endif /** binary_t_DEFINED */
callStatus_t _RD_demoHello(binary_t * _return, const char * txInput)
{
	cout << "SERVICE:RD_demoHello input argument:" << endl;
	if(txInput)
	{
		cout << "SERVICE:    txInput:" << txInput << endl;
	}
	else
	{
		cout << "SERVICE:    txInput:<null>" << endl;
	}
	memset(_return,0,sizeof(binary_t));
	auto hex = bytes_to_hex_string(txInput,strlen(txInput));
	cast(hex,*_return);
	return callStatus_Ok_c;
}
