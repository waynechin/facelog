/*
 * Copyright 2020 zytech
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "erpc_setup_server_tcp.h"
#include "erpc_server_tcp_transport.h"
#include "erpc_manually_constructed.h"

using namespace erpc;

////////////////////////////////////////////////////////////////////////////////
// Variables
////////////////////////////////////////////////////////////////////////////////

static ManuallyConstructed<ServerTCPTransport> s_transport;

////////////////////////////////////////////////////////////////////////////////
// Code
////////////////////////////////////////////////////////////////////////////////

erpc_transport_t erpc_transport_server_tcp_init(const char *host, uint16_t port,uint32_t sendTimeoutMills,uint32_t recvTimeoutMills)
{
    s_transport.construct(host,port);
    s_transport->timeout(sendTimeoutMills,recvTimeoutMills);
	erpc_status_t err = s_transport->open();
	if (err)
	{
		return NULL;
	}
    return reinterpret_cast<erpc_transport_t>(s_transport.get());
}

void erpc_transport_server_tcp_deinit()
{
	s_transport->close();
}
