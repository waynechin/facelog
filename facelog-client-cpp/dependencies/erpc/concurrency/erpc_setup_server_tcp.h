/*
 * Copyright (c) 2020, ZYtech, Inc.
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef _ERPC_SETUP_SERVER_TCP_H_
#define _ERPC_SETUP_SERVER_TCP_H_

#include "erpc_transport_setup.h"
/*!
 * @{
 * @brief erpc::TCPTransport类的标准C封装
 * @file
 */


////////////////////////////////////////////////////////////////////////////////
// API
////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

//! @name TCP/IP transport
//@{

/*!
 * @brief Create an TCP/IP transport.
 * 如果指定的主机和端口无法连接则返回NULL
 *
 * @param[in] host Specify the host name or IP address of the computer.
 * @param[in] port Specify the listening port number.
 * @param[in] sendTimeoutMills Specify timeout(milliseconds) for sending. ignore if less than or equal to zero
 * @param[in] recvTimeoutMills Specify timeout(milliseconds) for receiving. ignore if less than or equal to zero
 * @return Return NULL or erpc_transport_t instance pointer.
 */
erpc_transport_t erpc_transport_server_tcp_init(const char *host, uint16_t port,uint32_t sendTimeoutMills,uint32_t recvTimeoutMills);
/*!
 * @brief Deinitialize an TCP/IP transport.
 *
 * This function deinitializes the TCP/IP transport.
 */
void erpc_transport_server_tcp_deinit(void);

//@}

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _ERPC_SETUP_SERVER_TCP_H_
