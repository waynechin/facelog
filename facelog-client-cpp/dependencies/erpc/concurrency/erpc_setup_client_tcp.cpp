/*
 * Copyright 2020 zytech
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "erpc_setup_client_tcp.h"
#include "erpc_client_tcp_transport.h"
#include "erpc_manually_constructed.h"

using namespace erpc;

////////////////////////////////////////////////////////////////////////////////
// Variables
////////////////////////////////////////////////////////////////////////////////

static ManuallyConstructed<ClientTCPTransport> s_transport;

////////////////////////////////////////////////////////////////////////////////
// Code
////////////////////////////////////////////////////////////////////////////////

erpc_transport_t erpc_transport_client_tcp_init(const char *host, uint16_t port,uint32_t connTimeoutMills,uint32_t sendTimeoutMills,uint32_t recvTimeoutMills)
{
    s_transport.construct(host,port);
    s_transport->timeout(connTimeoutMills,sendTimeoutMills,recvTimeoutMills);
    return reinterpret_cast<erpc_transport_t>(s_transport.get());
}

void erpc_transport_client_tcp_deinit()
{
	s_transport->close();
}
