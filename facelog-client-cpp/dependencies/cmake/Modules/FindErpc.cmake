# FindErpc
# --------
#
# Find erpc libray
#
# Find the native erpc includes and library, this module defines
#
# ::
#
#   ERPC_INCLUDE_DIR, where to find header erpc, etc.
#   ERPC_FOUND, If false, do not try to use erpc.
#   ERPC_LIBRARIES, where to find the erpc library.
# import target:
# 	erpc

# handle the QUIETLY and REQUIRED arguments and set ERPC_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)

# find header file
find_path(ERPC_INCLUDE_DIR erpc_common.h PATH_SUFFIXES erpc)

find_library(ERPC_LIBRARIES erpc )
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ERPC DEFAULT_MSG ERPC_INCLUDE_DIR ERPC_LIBRARIES)
if(ERPC_FOUND)
	# for compatility of find_dependency
	set (Erpc_FOUND TRUE)
	add_library(erpc STATIC IMPORTED)
	message("-- IMPORTED STATIC library:erpc")
	set_target_properties(erpc PROPERTIES
		IMPORTED_LINK_INTERFACE_LANGUAGES "C CXX"
		INTERFACE_INCLUDE_DIRECTORIES ${ERPC_INCLUDE_DIR}
		IMPORTED_LOCATION "${ERPC_LIBRARIES}"
		)
	mark_as_advanced(ERPC_LIBRARIES ERPC_INCLUDE_DIR )
elseif(Erpc_FIND_REQUIRED)
	message(FATAL_ERROR "Erpc NOT FOUND")
endif(ERPC_FOUND)