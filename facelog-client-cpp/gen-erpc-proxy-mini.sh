#!/bin/bash
# 生成IFaceLog接口的erpc proxy实现代码(CPP)
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
OUT_FOLDER=src/erpc_proxy_mini
if [ -d $OUT_FOLDER ]
then
	rm "$OUT_FOLDER/facelog_erpc_cast.*"      >/dev/null 2>&1
	rm "$OUT_FOLDER/facelog_erpc_output.*"    >/dev/null 2>&1
	rm "$OUT_FOLDER/facelog_server_impl.cpp"  >/dev/null 2>&1
	rm "$OUT_FOLDER/facelog_proxy_server.cpp" >/dev/null 2>&1
fi
mvn com.gitee.l0km:codegen-thrift-maven-plugin:generate@erpc_proxy_mini || exit
popd