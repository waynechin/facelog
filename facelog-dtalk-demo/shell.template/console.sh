#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
java -jar ${project.build.finalName}-console-standalone.jar $*
popd