#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
java -Xrunjdwp:transport=dt_socket,server=y,address=8000,suspend=n -jar ${project.build.finalName}-console-standalone.jar $*
popd