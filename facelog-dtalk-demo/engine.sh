#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
target/$(basename $0) $*
popd
