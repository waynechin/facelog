package net.gdface.facelog;

/**
 * 模糊匹配结果条目
 * @author guyadong
 *
 */
public class MatchEntry{
	/**
	 * pattern匹配到的字符串
	 */
	private String key;
	/**
	 * 匹配的记录主键
	 */
	private int pk;
	public MatchEntry() {
	}
	public MatchEntry(int pk, String key) {
		super();
		this.pk = pk;
		this.key = key;
	}
	public int getPk() {
		return pk;
	}
	public void setPk(int pk) {
		this.pk = pk;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pk;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MatchEntry))
			return false;
		MatchEntry other = (MatchEntry) obj;
		if (pk != other.pk)
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MatchEntry [key=");
		builder.append(key);
		builder.append(", pk=");
		builder.append(pk);
		builder.append("]");
		return builder.toString();
	}
}