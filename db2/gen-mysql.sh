#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
cd $sh_folder 
sql2java_version=$(mvn org.apache.maven.plugins:maven-help-plugin:3.2.0:evaluate -Dexpression=sql2java.version -q -DforceStdout)
mvn com.gitee.l0km:sql2java-maven-plugin:$sql2java_version:generate -Dsql2java.classpath=lib/mysql-connector-java-5.1.43-bin.jar -Dsql2java.propfile=gen-mysql.properties || exit 255
mvn install
