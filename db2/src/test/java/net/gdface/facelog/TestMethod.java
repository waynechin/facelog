package net.gdface.facelog;

import java.lang.reflect.Method;
import java.util.Arrays;
import org.junit.Test;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import net.gdface.facelog.db.IDeviceManager;

import static com.google.common.base.Preconditions.*;
import static gu.sql2java.SimpleLog.*;

public class TestMethod {

	@Test
	public void test1() {
		log("int.class.name: {}",int.class.getName());
		log("int[].class.name: {},{}",int[].class.getName(),getTypeName(int[].class));
		log("byte[][].class.name: {},{}",byte[][].class.getName(),getTypeName(byte[][].class));
		log("Integer[].class.name: {},{}",Integer[].class.getName(),getTypeName(Integer[].class));
	}
	@Test
	public void test2() {
		int count = 0;
		for(Method method:IDeviceManager.class.getDeclaredMethods()){
			log("{}:{}",++count,signatureOf(method));
		}
	}
	@Test
	public void test3() {
		int count = 0;
		for(Method method:IDeviceManager.class.getMethods()){
			log("{}:{}",++count,signatureOf(method));
		}
	}
	/*
	 * Utility routine to paper over array type names
	 */
	public static final String getTypeName(Class<?> type) {
		if(null == type){
			return null;
		}else	if (type.isArray()) {
			return getTypeName(type.getComponentType()).concat("[]");
		} else{
			return type.getName().replaceAll("\\$", "\\.");
		}
			
	}
	public static final Function<Class<?>, String> TYPESTR_FUN= new  Function<Class<?>, String>(){
		@Override
		public String apply(Class<?> input) {
			return getTypeName(input);
		}		
	}; 
	public static final String signatureOf(Method method){
		checkNotNull(method,"method is null");
		return new StringBuilder()
			.append(method.getName())
			.append('(')
			.append(Joiner.on(',').join(Lists.transform(Arrays.asList(method.getParameterTypes()), TYPESTR_FUN)))
			.append(')').toString();
		
	}
}

