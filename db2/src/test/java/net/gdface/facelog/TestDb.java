package net.gdface.facelog;

import static gu.sql2java.Managers.registerCacheManager;
import static gu.sql2java.Managers.instanceOf;
import static gu.sql2java.Managers.getTableManager;
import static gu.sql2java.SimpleLog.*;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import gu.sql2java.Managers;
import gu.sql2java.SimpleLog;
import gu.sql2java.exception.ObjectRetrievalException;
import net.gdface.facelog.db.Constant;
import net.gdface.facelog.db.DeviceBean;
import net.gdface.facelog.db.DeviceGroupBean;
import net.gdface.facelog.db.FeatureBean;
import net.gdface.facelog.db.IDeviceGroupManager;
import net.gdface.facelog.db.IDeviceManager;
import net.gdface.facelog.db.IFeatureManager;
import net.gdface.facelog.db.IPermitManager;
import net.gdface.facelog.db.IPersonGroupManager;
import net.gdface.facelog.db.IPersonManager;
import net.gdface.facelog.db.PersonBean;
import net.gdface.facelog.db.PersonGroupBean;
import net.gdface.utils.BinaryUtils;

/**
 * @author guyadong
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING) 
public class TestDb implements Constant{
	private static IPersonManager personManager;
	private static IDeviceManager deviceManager ;
	private static IDeviceGroupManager deviceGroupManager ;
	private static IPersonGroupManager personGroupManager;
	private static IPermitManager permitManager ;
	private static IFeatureManager featureManager ;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// 允许输出调试信息
		Managers.setDebug(true);
		
		registerCacheManager("fl_device",Constant.UpdateStrategy.refresh,10000,60,TimeUnit.MINUTES);
		registerCacheManager("fl_device_group",Constant.UpdateStrategy.refresh,10000,60,TimeUnit.MINUTES);
		registerCacheManager("fl_face",Constant.UpdateStrategy.always,10000,10,TimeUnit.MINUTES);
		registerCacheManager("fl_image",Constant.UpdateStrategy.always,1000,10,TimeUnit.MINUTES);		 
		registerCacheManager("fl_person",Constant.UpdateStrategy.refresh,10000,10,TimeUnit.SECONDS);
		registerCacheManager("fl_person_group",Constant.UpdateStrategy.refresh,10000,60,TimeUnit.MINUTES);
		registerCacheManager("fl_store",Constant.UpdateStrategy.always,1000,10,TimeUnit.MINUTES);
		registerCacheManager("fl_feature",Constant.UpdateStrategy.refresh,10000,10,TimeUnit.MINUTES);
		registerCacheManager("fl_permit",Constant.UpdateStrategy.refresh,10000,10,TimeUnit.MINUTES);

		personManager = instanceOf(IPersonManager.class);
		deviceManager =instanceOf(IDeviceManager.class);
		deviceGroupManager = getTableManager(IDeviceGroupManager.class);
		personGroupManager = getTableManager(IPersonGroupManager.class);
		permitManager  = getTableManager(IPermitManager.class);
		featureManager = instanceOf(IFeatureManager.class);
		
		// TableManagerDecorator 测试
//		personManager = TableManagerDecorator.makeInterfaceInstance(IPersonManager.class, "fl_person");
//		deviceManager = TableManagerDecorator.makeInterfaceInstance(IDeviceManager.class,"fl_device");
//		deviceGroupManager = TableManagerDecorator.makeInterfaceInstance(IDeviceGroupManager.class,"fl_device_group");
//		personGroupManager = TableManagerDecorator.makeInterfaceInstance(IPersonGroupManager.class,"fl_person_group");
//		permitManager  = TableManagerDecorator.makeInterfaceInstance(IPermitManager.class,"fl_permit");
//		featureManager = TableManagerDecorator.makeInterfaceInstance(IFeatureManager.class,"fl_feature");
	
	}
	private DeviceBean makeDeviceBean(Integer id,String name){
		
		DeviceBean deviceBean = new DeviceBean();
		if(null !=id){
			deviceBean.setId(id);
		}
		deviceBean.setName(name);
		deviceBean.setGroupId(1);
		deviceBean.setUsedSdks("MTFSDK512");
		return deviceBean;
	}
	@Test
	public void test01ExistsPrimaryKey() {
		if(!deviceGroupManager.existsPrimaryKey(1)){
			DeviceGroupBean deviceGroup = new DeviceGroupBean(1);
			deviceGroup.setName("default_group");
			deviceGroupManager.save(deviceGroup);
		}
		DeviceBean b1 = makeDeviceBean(null,"hello");
		deviceManager.save(b1);
		log("SAVED " + b1);
		log("device id[{}] exists:{}",b1.getId(), deviceManager.existsPrimaryKey(b1.getId()));
		log("device id[null] exists:{}",deviceManager.existsPrimaryKey((Integer)null));
		DeviceBean reload = deviceManager.loadByPrimaryKey(b1.getId());
		logObjects(reload);
		log(reload.toString(true, false));
		log("all count:"+deviceManager.countAll());
	
		b1.setName("world");
		deviceManager.save(b1);
		logObjects(deviceManager.loadByWhereAsList("WHERE id="+b1.getId()));
		logObjects(deviceManager.countWhere("WHERE id="+b1.getId()));

		deviceManager.delete(b1);
		log("exist device {} {}",b1.getId(), deviceManager.existsByPrimaryKey(b1));
	}

	@Test
	public void test02SaveFeature(){
		
		byte[] feature = new byte[512];
		FeatureBean featureBean = FeatureBean.builder()
				.feature(ByteBuffer.wrap(feature))
				.md5(BinaryUtils.getMD5String(feature))
				.version("MTFSDK512")
				.build();
		if(!featureManager.existsByPrimaryKey(featureBean)){
			FeatureBean saved = featureManager.save(featureBean);
			System.out.println("SAVED " + saved);
			FeatureBean reloaded = featureManager.loadByPrimaryKey(saved);
			System.out.println("RELOAD " + reloaded);
		}
		featureManager.delete(featureBean);
		System.out.println("DELETED " + featureBean);
		System.out.println("COUNT " + featureManager.countAll());
	}
	
	@Test
	public void test03RunSql(){
		{
			List<Integer> result = deviceManager.runSqlAsList(Integer.class,"SELECT DISTINCT id FROM fl_device");
			System.out.println(result);
		}
		{
			List<Integer> result = deviceGroupManager.runSqlAsList(Integer.class,"SELECT DISTINCT id FROM fl_device_group");
			System.out.println(result);
		}
	}
	@Test
	public void test04LoadColumnAsList(){
		 List<Object> result = deviceManager.loadColumnAsList("used_sdks", true, null, 1, -1);
		 System.out.println(result);
	}
	@Test
	public void test05GetPersonsOfGroup(){
		{
			List<PersonBean> list = personGroupManager.getPersonBeansByGroupIdAsList(1);
			SimpleLog.logObjects(personManager.toPrimaryKeyList(list));
		}
		{
			for(PersonBean bean:personGroupManager.getPersonBeansByGroupIdAsList(1)){
				SimpleLog.logObjects(bean);
			}
		}
		{
			for(DeviceBean bean: deviceGroupManager.getDeviceBeansByGroupIdAsList(1)){
				SimpleLog.logObjects(bean);
			}
		}
	}
	@Test
	public void test06DeletePermit(){
		personGroupManager.addJunction(personGroupManager.loadByPrimaryKey(1), deviceGroupManager.loadByPrimaryKey(1));
		personGroupManager.deleteJunctionWithDeviceGroup(personGroupManager.loadByPrimaryKey(1), Arrays.asList(deviceGroupManager.loadByPrimaryKey(1)));
		permitManager.deleteByPrimaryKey(1,1);
	}
	
	@Test
	public void test07LoadViaPermitAsList(){
		personGroupManager.loadViaPermitAsList(DeviceGroupBean.builder().id(1).build());
	}
	@Test
	public void test08LoadByPrimaryKey() throws InterruptedException{
        ExecutorService executor = new ThreadPoolExecutor(
                8, 
                8,
                0,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(),
                new ThreadFactoryBuilder()
                    .setNameFormat("db-pool-%d")
                    .build());
        for(int i=0; i<200; ++i){
        	final int c=i;
        	executor.execute(new Runnable(){
        		@Override
        		public void run() {
        			SimpleLog.log("===============COUNT={}", c);
        			Managers.getSqlRunner(ALIAS_NAME).runAsTransaction(new Runnable() {
        				@Override
        				public void run() {
                			PersonBean bean = PersonBean.builder().name("chrome").build();
                			personManager.save(bean);
                			//Thread.sleep(12*1000);
                			PersonBean b2 = personManager.loadByPrimaryKey(bean.getId());
                			SimpleLog.logObjects(b2);        				
                		}
        			});
        		}});
        }
		executor.shutdown();
		while(!executor.isTerminated()){
			executor.awaitTermination(10, TimeUnit.SECONDS);
		}
	}
	@Test
	public void test09Transaction(){
		Managers.getSqlRunner(ALIAS_NAME).runAsTransaction(new Runnable() {
			@Override
			public void run() {
				test01ExistsPrimaryKey();
			}
		});
	}
	@Test
	public void test10LoadAll(){
		for(PersonBean person:Managers.instanceOf(IPersonManager.class).loadAll()){
			logObjects(person);
		}
		PersonBean b2 = personManager.loadByPrimaryKey(444);
		SimpleLog.logObjects(b2);   
	}
	@Test
	public void test11ListOfSelfRef(){
		PersonGroupBean g1 = PersonGroupBean.builder().name("g1").build();
		g1 = personGroupManager.save(g1);
		PersonGroupBean g2 = PersonGroupBean.builder().name("g2").parent(g1.getId()).build();
		g2 = personGroupManager.save(g2);
		PersonGroupBean g3 = PersonGroupBean.builder().name("g3").parent(g2.getId()).build();
		g3 =personGroupManager.save(g3);
		SimpleLog.logObjects(personGroupManager.toPrimaryKeyList(personGroupManager.listOfParent(g3)));   
		SimpleLog.logObjects(personGroupManager.isCycleOnParent(g3));
		try {
//			SimpleLog.logObjects(personGroupManager.topOfParent(g3));
			SimpleLog.log(personGroupManager.topOfParent(g3).getName());
		} catch (ObjectRetrievalException e) {
			SimpleLog.log("NOT FOUND PK");
		}
		
		SimpleLog.logObjects(personGroupManager.childListByParent(1));   
	}
	@Test
	public void test12ChildList(){	
		SimpleLog.logObjects(personGroupManager.childListByParent(1));   
	}
	@Test
	public void test12CacheGroup() throws InterruptedException{	
		   DeviceGroupBean deviceGroup1 = DeviceGroupBean.builder().name("test device group1").build();
		   deviceGroupManager.save(deviceGroup1);
		   DeviceGroupBean deviceGroup2 = DeviceGroupBean.builder().name("test device group2").build();
		   deviceGroupManager.save(deviceGroup2);
		   DeviceBean device = DeviceBean.builder().name("test device").usedSdks("MTFSDK512").groupId(deviceGroup1.getId()).build();
		   deviceManager.save(device);
		   log("device={}", device);
		   {
			   log("getDevice={}",deviceManager.loadByPrimaryKey(device.getId()));
		   }
		   device.setGroupId(deviceGroup2.getId());
		   deviceManager.save(device);
		   log("device={}", device);
		   
		   {
			   log("getDevice={}",deviceManager.loadByPrimaryKey(device.getId()));
		   }
	}
	@Test
	public void test13DeleteDeviceOnCache(){
		DeviceBean bean=DeviceBean.builder().usedSdks("MTFSDK512V2").mac("FF88FF88FF88").build();
		DeviceBean b2 = deviceManager.save(bean);
		deviceManager.deleteByPrimaryKey(bean.getId());
		log("exist bean {}",deviceManager.existsByPrimaryKey(bean));
		log("exist bean {}",null != deviceManager.loadByIndexMac("FF88FF88FF88"));

	}
	
}
