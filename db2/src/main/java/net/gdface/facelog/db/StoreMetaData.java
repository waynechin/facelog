// ______________________________________________________
// Generated by sql2java - https://github.com/10km/sql2java 
// JDBC driver used at code generation time: com.mysql.jdbc.Driver
// template: metadata.java.vm
// ______________________________________________________
package net.gdface.facelog.db;

import java.util.List;
import java.util.Arrays;
import gu.sql2java.RowMetaData;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableList;

/**
 * Class to supply table meta data for the fl_store table.<br>
 * @author guyadong
 */
public class StoreMetaData extends RowMetaData implements Constant
{  
    private static final ImmutableMap<String, String> JUNCTION_TABLE_PK_MAP = ImmutableMap.of();    
    private static final ImmutableList<String> FOREIGN_KEYS = 
        ImmutableList.<String>builder()
            .build();
    private static final ImmutableList<String> IMPORTED_FKNAMES = 
        ImmutableList.<String>builder()
            .build();
    private static final ImmutableList<String> INDEXS = 
        ImmutableList.<String>builder()
            .build();

    private static final List<String> GETTERS = Arrays.asList("getMd5","getEncoding","getData");

    private static final List<String> SETTERS = Arrays.asList("setMd5","setEncoding","setData");

    private static final String AUTO_INCREMENT_COLUMN = "";

    private static final Class<?> LOCK_COLUMN_TYPE = null;
    private static final String LOCK_COLUMN_NAME = null;
    public StoreMetaData(){
        super(
            "fl_store",
            "TABLE",
            StoreBean.class,
            "Store",
            IStoreManager.class,
            ALIAS_NAME,
            FL_STORE_FIELDS_LIST,
            FL_STORE_JAVA_FIELDS_LIST,
            GETTERS,
            SETTERS,
            FL_STORE_FIELD_TYPES,
            FL_STORE_FIELD_SIZES,
            FL_STORE_FIELD_SQL_TYPES,
            FL_STORE_PK_FIELDS_LIST,
            JUNCTION_TABLE_PK_MAP,
            LOCK_COLUMN_TYPE,
            LOCK_COLUMN_NAME,
            FOREIGN_KEYS,
            IMPORTED_FKNAMES,
            INDEXS,
            AUTO_INCREMENT_COLUMN
        );
    }
}
