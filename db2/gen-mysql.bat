@echo off
@set sh_folder=%~dp0
@pushd %sh_folder%
@call mvn org.apache.maven.plugins:maven-help-plugin:3.2.0:evaluate -Dexpression=sql2java.version -q -DforceStdout -Doutput=sql2java_version.log
@for /f %%i in ( sql2java_version.log ) do set sql2java_version=%%i
@call mvn com.gitee.l0km:sql2java-maven-plugin:%sql2java_version%:generate -Dsql2java.classpath=lib/mysql-connector-java-5.1.43-bin.jar -Dsql2java.propfile=gen-mysql.properties
@IF NOT ERRORLEVEL 0 EXIT -1
@call mvn install
@popd