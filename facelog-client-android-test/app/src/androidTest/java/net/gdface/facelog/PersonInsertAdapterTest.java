package net.gdface.facelog;

import android.support.test.runner.AndroidJUnit4;

import com.google.common.collect.ImmutableMap;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.exceptions.SmqUnsubscribeException;
import net.gdface.facelog.client.IFaceLogClient;
import net.gdface.facelog.thrift.IFaceLogThriftClient;
import net.gdface.thrift.ClientFactory;

/**
 * 数据更新频道订阅示例
 * @author guyadong
 *
 */
@RunWith(AndroidJUnit4.class)
public class PersonInsertAdapterTest implements CommonConstant {

	public static final Logger logger = LoggerFactory.getLogger(PersonInsertAdapterTest.class);

	@Test
	public void test() {
		final IFaceLogClient serviceClient = ClientFactory.builder().setHostAndPort("127.0.0.1", DEFAULT_PORT).build(IFaceLogThriftClient.class, IFaceLogClient.class);
		new SubAdapters.BasePersonInsertSubAdapter(){
			@Override
			public void onSubscribe(Integer id) throws SmqUnsubscribeException {
				logger.info("insert person ID:{}",id);
				logger.info("new recored {}",serviceClient.getPerson(id).toString(true, false));
			}
		}.register(MessageQueueFactorys.getDefaultFactory().getSubscriber());
	}

}
