package net.gdface.facelog.client;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import static org.junit.Assert.*;
import static com.google.common.base.Preconditions.checkArgument;
import static net.gdface.utils.BinaryUtils.getMD5String;

import net.gdface.facelog.CommonConstant;
import net.gdface.facelog.PersonDataPackage;
import net.gdface.facelog.Token;
import net.gdface.facelog.TopGroupInfo;
import net.gdface.facelog.db.DeviceBean;
import net.gdface.facelog.db.FaceBean;
import net.gdface.facelog.db.FeatureBean;
import net.gdface.facelog.db.ImageBean;
import net.gdface.facelog.db.PersonBean;
import net.gdface.facelog.thrift.IFaceLogThriftClient;
import net.gdface.thrift.ClientFactory;
import net.gdface.thrift.exception.ServiceRuntimeException;
import net.gdface.utils.BinaryUtils;
import net.gdface.utils.NetworkUtil;

/**
 * @author guyadong
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClientTest implements CommonConstant {
    public static final Logger logger = LoggerFactory.getLogger(ClientTest.class);

	private static IFaceLogClient facelogClient;
	private static Token rootToken;

	private static DeviceBean device;

	private static Token deviceToken;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// docker test
//		facelogClient = ClientFactory.builder().setHostAndPort("192.168.99.100", DEFAULT_PORT).build();
//		rootToken = facelogClient.applyRootToken("root", false);
		facelogClient = ClientFactory.builder()
				.setHostAndPort("127.0.0.1", DEFAULT_PORT)
				.setDecorator(RefreshTokenDecorator.makeDecoratorFunction(new TokenHelperTestImpl()))
				.build(IFaceLogThriftClient.class, IFaceLogClient.class);
		rootToken = facelogClient.applyRootToken("root", false);
		byte[] address = new byte[]{0x20,0x20,0x20,0x20,0x20,0x20};
		device = DeviceBean.builder().mac(NetworkUtil.formatMac(address, null)).serialNo("12322333").usedSdks("MTFSDKARM512").build();
		logger.info(device.toString(true,false));
		// 注册设备 
		device = facelogClient.registerDevice(device);
		logger.info("registered device {}",device.toString(true, false));
		// 申请设备令牌
		deviceToken = facelogClient.online(device);
		logger.info("device token = {}",deviceToken);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		facelogClient.unregisterDevice(deviceToken);
		facelogClient.releaseRootToken(rootToken);
	}

	@Test
	public void test1SavePerson() {
		PersonBean newPerson = PersonBean.builder().name("guyadong").mobilePhone("13611426411").build();
		try {
			newPerson = facelogClient.savePerson(newPerson,rootToken);
			logger.info("person = {}", newPerson.toString());
			PersonBean person = facelogClient.getPerson(newPerson.getId());
			PersonBean p1 = facelogClient.getPersonByMobilePhone(person.getMobilePhone());
			logger.info("person = {}", person.toString());
			facelogClient.deletePerson(person.getId(), rootToken);
			PersonBean p2 = facelogClient.getPersonByMobilePhone(person.getMobilePhone());
			logger.info("{}",p2 != null);
		} catch(ServiceRuntimeException e){
			e.printServiceStackTrace();
			assertTrue(false);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			assertTrue(false);
		}
	}
	@Test
	public void test2SavePersonWithImage() {
		PersonBean newPerson = PersonBean.builder().name("person1").build();
		try {			
			byte[] imgdata = BinaryUtils.getBytes(ClientTest.class.getResourceAsStream("/images/guyadong-3.jpg"));
			newPerson = facelogClient.savePerson(newPerson,imgdata,rootToken);
			logger.info("person = {}", newPerson.toString());
			PersonBean person = facelogClient.getPerson(newPerson.getId());
			logger.info("person = {}", person.toString());
			facelogClient.deletePerson(person.getId(), rootToken);
		} catch(ServiceRuntimeException e){
			e.printServiceStackTrace();
			assertTrue(false);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			assertTrue(false);
		}
	}
	@Test
	public void test3SavePersonWithImageAndFeature() {
		
		try {			
			PersonBean newPerson = PersonBean.builder().name("person2").build();
			byte[] imgdata = BinaryUtils.getBytes(ClientTest.class.getResourceAsStream("/images/guyadong-3.jpg"));
//			FaceBean facebean = FaceBean.builder().faceLeft(0).faceTop(100).faceWidth(200).faceHeight(200)
//					.imageMd5(BinaryUtils.getMD5String(imgdata))
//					.build();
			byte[] feature = new byte[]{1,1,3,1,1,};
			newPerson = facelogClient.savePerson(newPerson,imgdata,feature,null,(List<FaceBean>)null, deviceToken);
			logger.info("person = {}", newPerson.toString());
			PersonBean person = facelogClient.getPerson(newPerson.getId());
			logger.info("person = {}", person.toString());
			FeatureBean featureBean = facelogClient.getFeature(BinaryUtils.getMD5String(feature));
			logger.info("{}",featureBean);
			{
				// 添加一个新的特征
				byte[] feature2 = new byte[]{1,1,3,1,3,44};
				facelogClient.savePerson(person, null, feature2, null,(List<FaceBean>)null, deviceToken);
			}
			facelogClient.deletePerson(person.getId(), rootToken);
		} catch(ServiceRuntimeException e){
			e.printServiceStackTrace();
			assertTrue(false);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			assertTrue(false);
		}
	}
	@Test
	public void test2List(){
		try{
			List<Integer> persons = facelogClient.loadAllPerson();
			for(Integer id:persons){
				System.out.println(facelogClient.getPerson(id).toString(true, false));
			}
		}catch(ServiceRuntimeException e){
			e.printServiceStackTrace();
			assertTrue(false);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			assertTrue(false);

		}
	}
	@Test
	public void test3GetDeviceIdOfFeature(){
		try{
			Integer deviceId = facelogClient.getDeviceIdOfFeature(null);
			logger.info("{}",deviceId);
			List<Integer> devices = facelogClient.loadDeviceIdByWhere(null);
			for(Integer id:devices){
				logger.info(id.toString());
			}
		}catch(ServiceRuntimeException e){
			e.printServiceStackTrace();
			assertTrue(false);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			assertTrue(false);
		}
	}
	@Test
	public void test5AddImage(){
		try{
			URL url = this.getClass().getResource("/images/guyadong-3.jpg");
			byte[] imgBytes = BinaryUtils.getBytesNotEmpty(url);
			String md5 = Hashing.md5().hashBytes(imgBytes).toString();
			facelogClient.deleteImage(md5, rootToken);
			ImageBean imageBean = facelogClient.addImage(BinaryUtils.getBytesNotEmpty(url), null, null, 0, rootToken);
			logger.info("image added {}",imageBean.toString(true, false));
		}catch(ServiceRuntimeException e){
			e.printServiceStackTrace();
			assertTrue(false);
		}catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			assertTrue(false);
		}
	}

	@Test
	public void test5DeleteAll(){
		try{
			List<Integer> persons = facelogClient.loadAllPerson();			
			facelogClient.deletePersons(persons, rootToken);
			int count = facelogClient.countPersonByWhere(null);
			assertTrue(0 == count);
		}catch(ServiceRuntimeException e){
			e.printServiceStackTrace();
			assertTrue(false);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			assertTrue(false);
		}
	}
	@Test
	public void test6Version(){
		logger.info("service version:{}",facelogClient.version());
		logger.info("version detail:{}",facelogClient.versionInfo().toString());
		assertTrue("version ok",true);
	}
	
	@Test
	public void testPersonDataPackage(){
		try {
			List<PersonDataPackage> packages = facelogClient.loadPersonDataPackages(null, "MTFSDK512V2", 1);
			logger.info("packages size :{}",packages.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testMultiThread() throws InterruptedException{
        ExecutorService executor = new ThreadPoolExecutor(
                1, 
                1,
                0,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(),
                new ThreadFactoryBuilder()
                    .setNameFormat("cached-pool-%d")
                    .build());
		PersonBean newPerson = PersonBean.builder().name("guyadong").build();
		try {
			newPerson = facelogClient.savePerson(newPerson,rootToken);
			logger.info("person = {}", newPerson.toString());
		} catch(ServiceRuntimeException e){
			e.printServiceStackTrace();
			assertTrue(false);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			assertTrue(false);
		}
		final Integer id = newPerson.getId();
		for(int i=0;i<200;++i){
			executor.execute(new Runnable(){
				@Override
				public void run() {
					try {
						PersonBean person = facelogClient.getPerson(id);
						logger.info("person = {}", person.toString());
					} catch (Exception e) {
						e.printStackTrace();
					} 
				}});
		}
		executor.shutdown();
		while(!executor.isTerminated()){
			executor.awaitTermination(10, TimeUnit.SECONDS);
		}
	}
	public static class TokenHelperTestImpl extends TokenHelper {

		public TokenHelperTestImpl() {
		}

		@Override
		public String passwordOf(int id) {
			return "guyadong";
		}

		@Override
		public boolean isHashedPwd() {
			return false;
		}

	}

	private static TopGroupInfo createTestTopGroup(String topName) {
		TopGroupInfo topGroupInfo = new TopGroupInfo();
		topGroupInfo.setName(topName);
		ImmutableMap<String, String> nodes = ImmutableMap.<String, String>builder()
				.put("1楼/2单元","101/2,102,201,202")
				.put("2楼","8201房,8202房,8203房,8204房")
				.put("","8101房,8102房,8103房,8104房")
				.put("2楼/物业办公室","201,202,203")
				.put("停车场","")
				.put("东门","")
				.put("北门","")
				.build();
		topGroupInfo.setNodes(nodes);
		ImmutableMap<String, String> permits = ImmutableMap.<String, String>builder()
				.put("1楼/保洁组","1楼,*")
				.put("2楼/保洁组","2楼")
				.put("保安组",",东门,北门,停车场")
				.put("","停车场")
				.put("管理组","2楼/物业办公室,201,208")
				.build();
		topGroupInfo.setPermits(permits);
		facelogClient.initTopGroup(topGroupInfo, rootToken);
		return topGroupInfo;
	}
	private static void createTestTopGroupIfAbsent(String topName) {
		if(!existTopPersonGroup(topName)){
			createTestTopGroup(topName);
		}
	}
	private static boolean existTopPersonGroup(String topName){
		return null != topName && !facelogClient.loadPersonGroupIdByWhere(String.format("WHERE name='%s' AND parent IS NULL",topName), rootToken).isEmpty();
	}
	private static Integer getTopPersonGroup(String topName){
		if(null == topName ){
			return null;
		}
		List<Integer> tops = facelogClient.loadPersonGroupIdByWhere(String.format("WHERE name='%s' AND parent IS NULL",topName), rootToken);
		if(tops.isEmpty()){
			return null;
		}
		checkArgument(tops.size() == 1,"found multi person group with name {}",topName);
		return tops.get(0);
	}

	private static List<Integer> getPersonsByNameInTopGroup(String personName,String topName){
		if(null == personName || null == topName ){
			return Collections.emptyList();
		}
		List<Integer> tops = facelogClient.loadPersonGroupIdByWhere(String.format("WHERE name='%s' AND parent IS NULL",topName), rootToken);
		checkArgument(tops.size() != 0,"not found person group with name {}",topName);
		checkArgument(tops.size() == 1,"found multi person group with name {}",topName);
		List<Integer> groups = facelogClient.childListForPersonGroup(tops.get(0));
		return facelogClient.loadPersonIdByWhere(String.format("WHERE name='%s' AND group_id IN(%s)",personName,Joiner.on(',').join(groups)), rootToken);
	}

	private static PersonBean addAdminPersonInGroupIfAbsent(PersonBean person,String topName){
		if(null == person){
			return null;
		}
		checkArgument(!Strings.isNullOrEmpty(person.getName()),"person's name is null");
		List<Integer> found = getPersonsByNameInTopGroup(person.getName(),topName);
		if(!found.isEmpty()){
			checkArgument(found.size() ==1,"found multi person with name {}",person.getName());
			person =facelogClient.getPerson(found.get(0));
		}
		person.setRank(3);
		person.setGroupId(getTopPersonGroup(topName));
		return facelogClient.savePerson(person, rootToken);
	}
	@Test
	public void testInitTopGroup() {
		try{
			createTestTopGroup("家天下1");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCheckPermission01(){
		try {

			createTestTopGroupIfAbsent("家天下1");
			createTestTopGroupIfAbsent("家天下2");
			PersonBean a1 = addAdminPersonInGroupIfAbsent(PersonBean.builder().name("a1").rank(3).password(getMD5String("a1".getBytes())).build(),"家天下1");
			PersonBean a2 = addAdminPersonInGroupIfAbsent(PersonBean.builder().name("a2").rank(3).password(getMD5String("a2".getBytes())).build(),"家天下2");
			Token a1token = facelogClient.applyPersonToken(a1.getId(), "a1", false);
//			Token a2token = facelogClient.applyPersonToken(a2.getId(), "a2", false);
			facelogClient.deletePerson(a2.getId(), a1token);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
