package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("TmpwdTargetInfo")
public final class TmpwdTargetInfo
{
    public TmpwdTargetInfo() {
    }

    private Long expiryDate;

    @ThriftField(value=1, name="expiryDate", requiredness=Requiredness.OPTIONAL)
    public Long getExpiryDate() { return expiryDate; }

    @ThriftField
    public void setExpiryDate(final Long expiryDate) { this.expiryDate = expiryDate; }

    private int targetId;

    @ThriftField(value=2, name="targetId", requiredness=Requiredness.REQUIRED)
    public int getTargetId() { return targetId; }

    @ThriftField
    public void setTargetId(final int targetId) { this.targetId = targetId; }

    private TmpPwdTargetType targetType;

    @ThriftField(value=3, name="targetType", requiredness=Requiredness.OPTIONAL)
    public TmpPwdTargetType getTargetType() { return targetType; }

    @ThriftField
    public void setTargetType(final TmpPwdTargetType targetType) { this.targetType = targetType; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("expiryDate", expiryDate)
            .add("targetId", targetId)
            .add("targetType", targetType)
            .toString();
    }
}
