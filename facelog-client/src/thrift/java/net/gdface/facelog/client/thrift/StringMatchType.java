package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;

public enum StringMatchType
{
    EXACTLY_MATCH(0), CMP_LEFT_MATCH(1), CMP_RIGHT_MATCH(2), CMP_MATCH(3), WILDCARD_MATCH(4), DIGIT_FUZZY_MATCH(5), DIGIT_FUZZY_LEFT_MATCH(6), DIGIT_FUZZY_RIGHT_MATCH(7), DIGIT_SEP_FUZZY_MATCH(8), DIGIT_SEP_FUZZY_LEFT_MATCH(9), DIGIT_SEP_FUZZY_RIGHT_MATCH(10), REGEX_MATCH(11), FUZZY_MATCH(12);

    private final int value;

    StringMatchType(int value)
    {
        this.value = value;
    }

    @ThriftEnumValue
    public int getValue()
    {
        return value;
    }
}
