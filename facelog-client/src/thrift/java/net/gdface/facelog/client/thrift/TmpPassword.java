package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("TmpPassword")
public final class TmpPassword
{
    public TmpPassword() {
    }

    private Long expiryDate;

    @ThriftField(value=1, name="expiryDate", requiredness=Requiredness.OPTIONAL)
    public Long getExpiryDate() { return expiryDate; }

    @ThriftField
    public void setExpiryDate(final Long expiryDate) { this.expiryDate = expiryDate; }

    private String password;

    @ThriftField(value=2, name="password", requiredness=Requiredness.OPTIONAL)
    public String getPassword() { return password; }

    @ThriftField
    public void setPassword(final String password) { this.password = password; }

    private int targetId;

    @ThriftField(value=3, name="targetId", requiredness=Requiredness.REQUIRED)
    public int getTargetId() { return targetId; }

    @ThriftField
    public void setTargetId(final int targetId) { this.targetId = targetId; }

    private TmpPwdTargetType targetType;

    @ThriftField(value=4, name="targetType", requiredness=Requiredness.OPTIONAL)
    public TmpPwdTargetType getTargetType() { return targetType; }

    @ThriftField
    public void setTargetType(final TmpPwdTargetType targetType) { this.targetType = targetType; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("expiryDate", expiryDate)
            .add("password", password)
            .add("targetId", targetId)
            .add("targetType", targetType)
            .toString();
    }
}
