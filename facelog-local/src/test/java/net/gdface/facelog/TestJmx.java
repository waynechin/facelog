package net.gdface.facelog;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.Map;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.activemq.broker.jmx.BrokerViewMBean;
import org.junit.Test;

import static gu.sql2java.SimpleLog.*;

public class TestJmx {

	@Test
	public void test() {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		String name = "org.apache.activemq:type=Broker,brokerName=localhost";
		try { 

			ObjectName objectName = new ObjectName(name);
			//			if (mbs.isRegistered(objectName)) {
			//				String port = System.getProperty("com.sun.management.jmxremote.port");
			//				System.out.printf("com.sun.management.jmxremote.port=%s",port);
			JMXServiceURL url =
					new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:1099/jmxrmi");
			JMXConnector jmxc = JMXConnectorFactory.connect(url, null);
			MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
			BrokerViewMBean mbean =
					(BrokerViewMBean)MBeanServerInvocationHandler.newProxyInstance(
							mbsc,objectName,BrokerViewMBean.class,true);
			Map<String, String> transportConnectors = mbean.getTransportConnectors();
			log("TransportConnectors {}",transportConnectors);
			//		}
		} catch (MalformedObjectNameException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
