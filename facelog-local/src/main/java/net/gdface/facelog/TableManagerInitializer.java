package net.gdface.facelog;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.EnumMap;
import java.util.concurrent.TimeUnit;

import gu.sql2java.Managers;
import net.gdface.utils.InterfaceDecorator;
import gu.sql2java.Constant.JdbcProperty;
import gu.sql2java.Constant.UpdateStrategy;

/**
 * 全局初始化数据库访问对象(TableManager)
 * @author guyadong
 *
 */
public class TableManagerInitializer implements ServiceConstant {
	static final boolean coreDebug; 
	private static final String PROP_UPDATESTRATEGY = "UpdateStrategy";
	private static final String PROP_MAXIMUMSIZE = "maximumSize";
	private static final String PROP_DURATION = "duration";
	private static final String PROP_ENABLE = "enable";
	static{
		coreDebug = CONFIG.getBoolean(SYSLOG_OP_COREDEBUG, false);
		// 允许输出调试信息
		if(coreDebug){
			Managers.setDebug(true);
			InterfaceDecorator.setDebug(true);
		}
		// 使用当前项目使用的数据库连接配置创建数据库连接实例
		EnumMap<JdbcProperty, String> databaseConfig = GlobalConfig.makeDatabaseConfig();
		Managers.createInstance(databaseConfig);
		TokenContext.setErrorDetail(CONFIG.getBoolean(SYSLOG_OP_TOKEN_ERRORDETAIL,false));

		// 根据配置cache
		if(enableOf("fl_device")){
			Managers.registerCacheManager("fl_device",updateStrategyOf("fl_device"),maximumSizeOf("fl_device"),durationOf("fl_device"),TimeUnit.MINUTES);
		}
		if(enableOf("fl_device_group")){
			Managers.registerCacheManager("fl_device_group",updateStrategyOf("fl_device_group"),maximumSizeOf("fl_device_group"),durationOf("fl_device_group"),TimeUnit.MINUTES);
		}
		if(enableOf("fl_face")){
			Managers.registerCacheManager("fl_face",updateStrategyOf("fl_face"),maximumSizeOf("fl_face"),durationOf("fl_face"),TimeUnit.MINUTES);
		}
		if(enableOf("fl_image")){
			Managers.registerCacheManager("fl_image",updateStrategyOf("fl_image"),maximumSizeOf("fl_image"),durationOf("fl_image"),TimeUnit.MINUTES);
		}
		if(enableOf("fl_person")){
			Managers.registerCacheManager("fl_person",updateStrategyOf("fl_person"),maximumSizeOf("fl_person"),durationOf("fl_person"),TimeUnit.MINUTES);
		}
		if(enableOf("fl_person_group")){
			Managers.registerCacheManager("fl_person_group",updateStrategyOf("fl_person_group"),maximumSizeOf("fl_person_group"),durationOf("fl_person_group"),TimeUnit.MINUTES);
		}
		if(enableOf("fl_store")){
			Managers.registerCacheManager("fl_store",updateStrategyOf("fl_store"),maximumSizeOf("fl_store"),durationOf("fl_store"),TimeUnit.MINUTES);
		}
		if(enableOf("fl_feature")){
			Managers.registerCacheManager("fl_feature",updateStrategyOf("fl_feature"),maximumSizeOf("fl_feature"),durationOf("fl_feature"),TimeUnit.MINUTES);
		}
		if(enableOf("fl_permit")){
			Managers.registerCacheManager("fl_permit",updateStrategyOf("fl_permit"),maximumSizeOf("fl_permit"),durationOf("fl_permit"),TimeUnit.MINUTES);
		}
		if(enableOf("fl_log")){
			Managers.registerCacheManager("fl_log",updateStrategyOf("fl_log"),maximumSizeOf("fl_log"),durationOf("fl_log"),TimeUnit.MINUTES);
		}
	}
	/**
	 * 读取指定表的cache相关属性
	 * @param tablename 表名
	 * @param prop 属性名
	 * @return 返回属性值字符串，如果未定义则返回默认值
	 */
	private static String getPropertyOfCache(String tablename,String prop){
		String defPropName = PREFIX_DATABASE + "cache.default." + prop;		
		String defaultValue = checkNotNull(CONFIG.getString(defPropName),"NO EXISTS property %s",defPropName);
		return CONFIG.getString(PREFIX_DATABASE  + "cache." + tablename + "." + prop, defaultValue);
	}
	private static UpdateStrategy updateStrategyOf(String tablename){
		return UpdateStrategy.valueOf(getPropertyOfCache(tablename,PROP_UPDATESTRATEGY));
	}
	private static long maximumSizeOf(String tablename){
		return Long.valueOf(getPropertyOfCache(tablename,PROP_MAXIMUMSIZE));
	}
	private static long durationOf(String tablename){
		return Long.valueOf(getPropertyOfCache(tablename,PROP_DURATION));
	}
	private static boolean enableOf(String tablename){
		return Boolean.valueOf(getPropertyOfCache(tablename,PROP_ENABLE));
	}
	public static final TableManagerInitializer INSTANCE = new TableManagerInitializer();
	private TableManagerInitializer() {
	}	
	
	public void init(){
	}

}
