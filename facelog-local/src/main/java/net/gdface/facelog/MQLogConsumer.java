package net.gdface.facelog;

import static com.google.common.base.Preconditions.checkNotNull;

import gu.simplemq.IConsumer;
import gu.simplemq.IMessageAdapter;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.exceptions.SmqUnsubscribeException;
import net.gdface.facelog.db.LogBean;
import net.gdface.facelog.BaseDao;

/**
 * 负责验证日志服务端统一保存的消费实现(未完成)
 * @author guyadong
 *
 */
class MQLogConsumer implements ServiceConstant {
	private final IConsumer consumer;
	private final BaseDao dao;
	public MQLogConsumer(BaseDao dao){
		this(dao,MessageQueueFactorys.getDefaultFactory());
	}
	public MQLogConsumer(BaseDao dao,IMessageQueueFactory factory){
		this.dao = checkNotNull(dao);
		this.consumer = checkNotNull(factory,"factory is null").getConsumer();
		this.consumer.register(ChannelConstant.QUEUE_LOG.asMutable().setAdapter(new IMessageAdapter<LogBean>(){
			@Override
			public void onSubscribe(final LogBean t) throws SmqUnsubscribeException {
				GLOBAL_EXCEUTOR.execute(new Runnable(){
					@Override
					public void run() {
						try{
							MQLogConsumer.this.dao.daoAddLog(t);
						}catch(Exception e){
							logger.error(e.getMessage(),e);
						}						
					}});
			}}));
	}
}
