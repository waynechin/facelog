package net.gdface.facelog;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.google.common.collect.ImmutableMap;

/**
 * version information for project com.gitee.l0km:dtalk-base
 * @author guyadong
 */
public final class Version {
    /** project version */
    public static final String VERSION;
    /** SCM(git) revision */
    public static final String SCM_REVISION;
    /** SCM branch */
    public static final String SCM_BRANCH;
    /** build timestamp */
    public static final String TIMESTAMP;
    /** map of version fields */
    public static final ImmutableMap<String,String> INFO ;
    static {
    	try(InputStream is = Version.class.getResourceAsStream("/version.properties")){
    		Properties properties=new Properties();
    		properties.load(is);
    		VERSION=properties.getProperty("VERSION");
    		SCM_REVISION=properties.getProperty("SCM_REVISION");
    		SCM_BRANCH=properties.getProperty("SCM_BRANCH");
    		TIMESTAMP=properties.getProperty("TIMESTAMP");
    		INFO = ImmutableMap.of(
    	    		"VERSION", VERSION, 
    	    		"SCM_REVISION", SCM_REVISION, 
    	    		"SCM_BRANCH",SCM_BRANCH,
    	    		"TIMESTAMP", TIMESTAMP);
    	} catch (IOException e) {
			throw new ExceptionInInitializerError(e);
		}
    }
}