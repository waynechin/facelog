package net.gdface.facelog;

import static com.google.common.base.MoreObjects.firstNonNull;
import static net.gdface.utils.ConditionChecks.checkTrue;
import javax.annotation.Nullable;

import net.gdface.facelog.ServiceSecurityException.SecurityExceptionType;

/**
 * 安全检查工具类
 * @author guyadong
 *
 */
public class SecurityChecks {

	private SecurityChecks() {
	}
	/**
	 * 执行表达式,为false时抛出{@link ServiceSecurityException}异常
	 * @param b
	 * @param type 异常类型
	 * @param errorMessageTemplate a template for the exception message should the check fail. The
	 *     message is formed by replacing each {@code %s} placeholder in the template with an
	 *     argument. These are matched by position - the first {@code %s} gets {@code
	 *     errorMessageArgs[0]}, etc. Unmatched arguments will be appended to the formatted message in
	 *     square braces. Unmatched placeholders will be left as-is.
	 * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
	 *     are converted to strings using {@link String#valueOf(Object)}.
	 * @throws ServiceSecurityException
	 */
	public static void checkSecurity(
			boolean b, 
			SecurityExceptionType type,
			@Nullable String errorMessageTemplate, 
			@Nullable Object... errorMessageArgs) throws ServiceSecurityException {
		checkTrue(b,ServiceSecurityException.class,firstNonNull(type,SecurityExceptionType.UNCLASSIFIED),errorMessageTemplate, errorMessageArgs);
	}
	/**
	 * 执行表达式,为false时抛出{@link ServiceSecurityException}异常
	 *
	 * <p>See {@link net.gdface.utils.ConditionChecks#checkTrue(boolean, Class, String, Object)} for details.
	 * @throws ServiceSecurityException 
	 */
	public static void checkSecurity(
			boolean b, SecurityExceptionType type,
			@Nullable String errorMessageTemplate, @Nullable Object p1) throws ServiceSecurityException {
		checkTrue(b,ServiceSecurityException.class,firstNonNull(type,SecurityExceptionType.UNCLASSIFIED),errorMessageTemplate, p1);
	}
}
