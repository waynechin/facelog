package net.gdface.facelog;

/**
 * 封装所有调用 FaceApi 相关方法时抛出的异常
 * @author guyadong
 *
 */
public class FaceApiRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public FaceApiRuntimeException(String message) {
		super(message);
	}

	public FaceApiRuntimeException(Throwable cause) {
		super(cause);
	}

}
