package net.gdface.facelog;

import static com.google.common.base.Preconditions.*;

import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IPublisher;
import gu.simplemq.MessageQueueFactorys;
import gu.sql2java.TableListener;
import gu.sql2java.exception.RuntimeDaoException;
import net.gdface.facelog.db.PersonGroupBean;

/**
 * 人员组表({@code fl_person_group})变动侦听器<br>
 * 当{@code fl_person_group}记录增删改时发布订阅消息
 * @author guyadong
 *
 */
class MQPersonGroupListener extends TableListener.Adapter<PersonGroupBean> implements ChannelConstant{

	private final IPublisher publisher;
	private final BaseDao dao;
	private PersonGroupBean beforeUpdatedBean;
	public MQPersonGroupListener(BaseDao dao) {
		this(dao, MessageQueueFactorys.getDefaultFactory());
	}
	public MQPersonGroupListener(BaseDao dao, IMessageQueueFactory factory) {
		this.dao = checkNotNull(dao,"dao is null");
		this.publisher = checkNotNull(factory,"factory is null").getPublisher();
	}
	@Override
	public void afterInsert(PersonGroupBean bean) {
		new PublishTask<Integer>(
				PUBSUB_PERSONGROUP_INSERT, 
				bean.getId(), 
				publisher)
		.execute();
	}
	@Override
	public void beforeUpdate(PersonGroupBean bean) throws RuntimeDaoException {
		// 保留更新前的数据
		beforeUpdatedBean = dao.daoGetPersonGroup(bean.getId()).clone();
	}
	@Override
	public void afterUpdate(PersonGroupBean bean) {
		// beforeUpdatedBean 为 null，只可能因为侦听器是被异步调用的
		checkState(beforeUpdatedBean != null,"beforeUpdatedBean must not be null");
		// 保存修改信息
		beforeUpdatedBean.setModified(bean.getModified());
		new PublishTask<PersonGroupBean>(
				PUBSUB_PERSONGROUP_UPDATE, 
				beforeUpdatedBean, 
				publisher)
		.execute();
		beforeUpdatedBean = null;
	}

	@Override
	public void afterDelete(PersonGroupBean bean) {
		new PublishTask<PersonGroupBean>(
				PUBSUB_PERSONGROUP_DELETE, 
				bean, 
				publisher)
		.execute();
	}			

}
