#!/bin/bash
# 生成 FaceLog csharp client代码脚本
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd "$sh_folder"
OUT_FOLDER="$sh_folder/../facelog-client-ext/csharp"
# 指定thrift compiler位置
[ $(which thrift) >/dev/null ] && THRIFT_EXE=thrift
[ -z "$THRIFT_EXE" ] && THRIFT_EXE=$sh_folder/dependencies/dist/thrift-$(g++ -dumpmachine)/bin/thrift
$THRIFT_EXE --version || exit

[   -e "$OUT_FOLDER" ] && rm "$OUT_FOLDER/*.cs" >/dev/null 2>/dev/null
[ ! -e "$OUT_FOLDER" ] && rm -fr "$OUT_FOLDER"

$THRIFT_EXE --gen csharp:nullable,async \
	-out "$OUT_FOLDER" \
	"$sh_folder/IFaceLog.thrift" || exit 

popd
