#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
export FACELOG_DEBUG_OPTS=-Xrunjdwp:transport=dt_socket,server=y,address=8000,suspend=n
$sh_folder/start_facelog_server.sh $*