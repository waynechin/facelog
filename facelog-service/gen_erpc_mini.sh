#!/bin/bash
# 生成 facelog eRPC 剪裁接口定义文件(IDL)
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd "$sh_folder"
mvn com.gitee.l0km:swift2thrift-maven-plugin:generate@erpc_mini $* || exit
popd
